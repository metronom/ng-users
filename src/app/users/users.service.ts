import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  getUsers = () => this.http.get('users');

  getUser = (id: string|number) => this.http.get(`/user/${id}`);
}
