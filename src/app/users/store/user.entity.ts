import { createEntityAdapter, EntityState } from '@ngrx/entity';

export interface UserModel {
  id: string;
  username: string;
}

export const userAdapter = createEntityAdapter<UserModel>();

export interface UserState extends EntityState<UserModel> { }

