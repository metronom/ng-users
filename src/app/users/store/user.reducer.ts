import {
  UserActionTypes,
} from './user.actions';
import {userAdapter, UserState} from './user.entity';

const initialState: UserState = userAdapter.getInitialState();

export const UserReducer = (
  state: UserState = initialState,
  action
): UserState => {
  switch (action.type) {
    case UserActionTypes.SET_USERS:
      return userAdapter.addAll(action.users, state);
  }
}
