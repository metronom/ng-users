import { Action } from '@ngrx/store';
import { UserModel } from './user.entity';

export enum UserActionTypes {
  SET_USERS = '[USER]/GET_ALL',
}

export class SetUsers implements Action {
  readonly type = UserActionTypes.SET_USERS;

  constructor(public users: UserModel[]) { }
}

export type UserAction = SetUsers;
